package nl.utwente.di.bookQuote;

public class Quoter {
    double getFahrenheit(double celsius) {
        return celsius * 9 / 5 + 32;
    }
}
